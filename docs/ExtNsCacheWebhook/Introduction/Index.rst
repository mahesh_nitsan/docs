
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_cache_webhook
====================

.. _What-does-it-do:

What does it do?
================

Add option at Flush cache menu call "Flush Webhook Cache" at TYPO3 backend. Call your Webhook configured at Settings > Configure extensions > EXT.ns_cache_webhook. Read more at Configuration and Usage sections.

Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/cache-webhook-typo3-extension
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support