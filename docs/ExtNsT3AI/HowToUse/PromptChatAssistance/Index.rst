.. include:: ../Includes.txt


=========================
Backend Modules T3AI
=========================

The backend module of T3AI comes with two modules.

Helpful Prompts
====================

This Module comes up with 100+ Predefined Prompts that helps you to not only create but also optimise content as per your requirements.

.. figure:: Images/promptfinal.png
   :alt: promptfinal


Chat Assistance 
====================

This module allows you to engage in natural language conversations with ChatGPT directly from your TYPO3 backend to receive quick answers to your questions and gather content insights.

.. figure:: Images/chat_assi.png
   :alt: chat_assi

