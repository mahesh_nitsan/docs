
.. include:: ../Includes.txt

============
Introduction
============


EXT:ns_t3ai
=================

.. figure:: Images/t3-openai-banner.jpeg
   :alt: Extension Banner


What does it do?
================

T3AI is the first-ever ChatGPT (by OpenAI) Extension that helps you to create feature-rich content and optimise the same..T3AI allows you to create seo-optimized meta titles, descriptions, keywords and social media tags. It helps you create AI pages, from scratch. Furthermore, translate the pages from one language to another language with a single click.

Additionally, T3AI offers 100+ predefined prompts that assist you in effortlessly creating engaging content, with dedicated chat assistance. In short it is your one stop destination for everything AI. 


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com//t3ai-typo3-chatgpt-extension
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-ns-t3-ai
	- Front End Demo: https://demo.t3planet.com/t3-extensions/pro/t3ai-typo3-chatgpt-extension
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support