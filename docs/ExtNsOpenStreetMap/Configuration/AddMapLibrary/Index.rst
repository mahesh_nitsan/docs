.. include:: ../Includes.txt



==================
Add OpenStreet Map Library
==================

First of all, you need to add OpenStreet Map Library.

You can do it by performing following steps:

Step 1: Go to TYPO3 Template Module > Select the page in which you want to configure the OpenStreet Map Extension.

Step 2: Go to the Constant Editor.

Step 3: Choose OpenStreet Map plugin from the Category dropdown box.

Step 4: Now, add OpenStreet Map Library key on the "Others" Setting.

.. figure:: Images/openStreetMap_Leaflet_Library.jpeg
   :alt: openstreet_map_Leaflet_Library

Now you can Save the changes & use it on your awesome website.!
