.. include:: ../Includes.txt

.. _faq:

==================
Get This Extension
==================

Get this extension from https://t3planet.com/lazy-load-typo3-extension-free or https://extensions.typo3.org/extension/ns_lazy_load

