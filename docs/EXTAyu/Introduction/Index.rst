
.. include:: ../Includes.txt

============
Introduction
============

.. figure:: Images/T3Ayu_banner.png
   :alt: T3Planet-TYPO3-Theme-T3Ayu

ns_theme_t3ayu
================
      		 
T3 Ayu our TYPO3 Headless Template, meticulously designed with the dynamic synergy of React and Next.js.

Extensions Dependencies
=======================

- ns_basetheme
- blog
- headless
- ns_headless_blog
- headless_container
- ns_headless_mask

**Note:** During the installation, You need to install EXT: headless_news manually from the github. Please check it from here - https://github.com/TYPO3-Initiatives/headless_news


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/t3-ayu-reactjs-typo3-template
	- Front End Demo: https://demo.t3planet.com/?theme=t3t-ayu
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support