﻿.. include:: Includes.txt

===================
EXT:ns_personio
===================

.. toctree::
   :glob:

   Introduction/Index
   Installation/Index
   UpdateVersion/Index
   ConfigurePersonioAPIandScheduler/Index
   ConfigurationOfPlugins/Index
   Support
   BuyNow
   