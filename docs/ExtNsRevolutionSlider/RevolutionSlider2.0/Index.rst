.. include:: Includes.txt

====================
Slider Revolution v2
====================

.. toctree::
   :glob:

   MigrationFrom1to2/Index
   Installation/Index
   Configuration/Index