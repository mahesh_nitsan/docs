
.. include:: ../Includes.txt

============
Introduction
============


EXT:ns_cookies
==============

.. figure:: Images/typo3-ext-cookies_hint.jpg
   :alt: Extension Banner


What does it do?
================

Do you want to add a cookies consent popup in your TYPO3 website? The European Union’s cookie law requires websites to get user consent to set any cookies on their website. As a TYPO3 website owner, your website may be using cookies as well. The Cookie Hint plugin will assist you in making your website GDPR compliant.


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/simple-cookie-bar-typo3-extension
	- Front End Demo: https://demo.t3planet.com/t3-extensions/cookies
	- Typo3 Back End Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/index.php?route=%2Fmain&token=eeeccfd7bfeef6115b6f4bd22b9105668a42f5f6
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support