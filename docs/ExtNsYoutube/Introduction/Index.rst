
.. include:: ../Includes.txt

============
Introduction
============

ns_youtube
==========

.. figure:: Images/TYPO3_NS_YOUTUBE_NITSAN_Banner_Preview.jpg
   :alt: TYPO3 EXT:NS_youtube Banner 
   

.. _What-does-it-do:

What does it do?
================

One of the only TYPO3 extension which provides to integrate all the features of Youtube.com.


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/youtube-typo3-extension-free
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-youtube
	- Front End Demo: https://demo.t3planet.com/t3-extensions/youtube
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support