.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Preconditions for Migration:
===========================

We assume you have EXT:grid elements and EXT:container Extension.For migration you must set Container Configuration same as Grid Configuration !

When you make the container configuration then some points are strict to follow for migration process
**Container CType** and Old Gridelements **tx_gridelements_backend_layout** the key must be the same.

**For Example :** I need to migrate the Grid of **"nsBase1Col"** then while in container configuration, you need to put container's CType as **"nsBase1Col"**


Grid Backend Layout Key
***********************
        .. figure:: Images/Grid_Backend_Layout_Key.jpg
                :alt: Grid Backend Layout Key
                :width: 1300px

Container CType
***************
        .. figure:: Images/containerCtype.jpg
                :alt: Container CType
                :width: 1300px

**For colPos > Grid colPos:** For example, the Grid's Colpos is '1' then while in container configuration you need to put colPos higher than Grid. like '101'
Because In migration process, we make the same container's colpos with which comes of grid's colPos for the placing Content Elements of Grid in Container while migrating.
Like. Grid ColPos** '1'[tx_gridelements_columns] and in container configuration, colPos is '101'**

Then in migrating process, we make same colPos for placing Content Elements $colPos = $element['tx_gridelements_columns'] + 100;"

        .. figure:: Images/Grid_colpos.jpg
                :alt: Grid_colposs
                :width: 1300px

While container colPos must be different like
        .. figure:: Images/Container_colpos.jpg
                :alt: container colPos
                :width: 1300px

We provide 2 options for migrating grids
========================================

**Option 1:Migration of all Grids available on the site.**
        .. figure:: Images/Migration.Jpg
                :alt: Migration
                :width: 1300px

**Option 2:If you have a small number of grids then you can use the second option "Migration from grid elements layout key"**
        .. figure:: Images/All_grid_layout.jpg
                :alt: All_grid_layout
                :width: 1300px

Migration Process:
==================

**Grid:** For example You want to migrat this gird
        .. figure:: Images/grid.jpg
                :alt: Grid
                :width: 1300px
**Follow this Steps>>**

**Step 1:** Enter container CType with same as Grid Backend Layout Key
        .. figure:: Images/Grid_Backend_Layout_Key.jpg
                :alt: Grid_Layout_key
                :width: 1300px

**Step 2:** Click on "Migrate Button" It will show message like "Successfully migrated"
        .. figure:: Images/Success_msg.jpg
                :alt: Success_Msg
                :width: 1300px

**Step 3:** Now, check the migration!
        .. figure:: Images/Migration_Done.Jpg
                :alt: Migration_Done
                :width: 1300px

**This Extension also Provide feature to migrate Hidden content Elements!** 
        .. figure:: Images/hidden_1.jpg
                :alt: Hidden_1
                :width: 1300px

        .. figure:: Images/hidden_2.jpg
                :alt: Hidden_2
                :width: 1300px
 
**That's it, Now you can enjoy all the benifits of this extension :)**