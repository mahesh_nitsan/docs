.. include:: ../Includes.txt

=============
Theme Options
=============

Global-Level Configuration
==========================

This module will help you configure global settings.

- Go to NITSAN > Theme Options
- Click on "Root/Main" page from Page-Tree
- You can configure General, SEO, GDPR, Style, Integration etc.
- All the theme options are self-explain, We recommend to configure everything once.

.. figure:: Images/T3Terminal-T3Bootstrap-Theme-OptionsGeneral.jpeg
   :alt: T3Planet-T3Bootstrap-Theme-OptionsGeneral

.. figure:: Images/T3Terminal-T3Bootstrap-Theme-OptionsSEO.jpeg
   :alt: T3Planet-T3Bootstrap-Theme-OptionsSEO

.. figure:: Images/T3Terminal-T3bootstrap-Theme-OptionsGDPR.jpeg
   :alt: T3Planet-T3bootstrap-Theme-OptionsGDPR
   
.. figure:: Images/T3Terminal-T3Bootstrap-Theme-OptionsStyle.jpeg
   :alt: T3Planet-T3Bootstrap-Theme-OptionsStyle

.. figure:: Images/T3Terminal-T3Bootstrap-Theme-OptionsIntegration.jpeg
   :alt: T3Planet-T3Bootstrap-Theme-OptionsIntegration
   

**General** : This tab consists of settings related to Header, Navigation menu, Footer & Site Maintenance.

**SEO** : This tab consist configurations related to SEO of the site.

**GDPR** : This tab manages the GDPR Cookiebar settings and its style.

**Style** : You can manage style of entire site from here. You can set global settings for elements like Header, Navigation Menu & footer from here. However, you can overwrite them at page level also. To overwrite these settings, go to Extended tab in Page properties.

**Integration** : You can add customised CSS and Integration scripts from third-party tools for Tracking purposes.


Page-Level Configuration
========================

From theme option tab, you can now directly select page, and create a extension template & configure desired style for particular page.

.. figure:: Images/t3bootstrap-create-extension-template.jpeg
   :alt: t3bootstrap-create-extension-template
   
After creating extension template for the specific page, you can now configure all theme options for this particular page only.

.. figure:: Images/t3bootstrap-page-level-theme-option.jpeg
   :alt: t3bootstrap-page-level-theme-option