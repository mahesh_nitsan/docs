.. include:: ../Includes.txt

=============
Configuration
=============

Get Domain Group ID
===================

**Step 1:** Go to https://www.cookiebot.com/en/ and Login.

**Step 2:** Navigate to Domain Tab. Add Domain by clicking on “+” button.

.. figure:: Images/config-1.jpg
   		:alt: Cookiebot Configuration

**Step 3:** Navigate to Dialog tab. Set Template of cookiebar. You can also configure other settings like Method, Page scroll, Type, Checkboxes default mode, etc.

.. figure:: Images/config-2.jpg
   		:alt: Cookiebot Configuration

**Step 4:** Go to the Content tab. Set the content to display in Cookies introduction text for all selected Cookies categories.

.. figure:: Images/config-3.jpg
   		:alt: Cookiebot Configuration

**Step 5:** Switch to Your scripts tab. There you would find the Domain Group ID for your domain.

.. figure:: Images/config-4.jpg
   		:alt: Cookiebot Configuration

Configure Domain Group ID in your website
=========================================

Set this Domain Group ID at TYPO3 constant editor, Check below screenshot

.. figure:: Images/config-5.jpeg
   		:alt: Cookiebot Configuration



