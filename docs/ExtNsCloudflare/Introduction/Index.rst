	
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_cloudflare
==================

.. figure:: Images/ext_cloudflare.png
   :alt: Extension Cookibot Banner 


What does it do?
================

EXT:ns_cloudflare extenion cache management becomes a straightforward and efficient process, helping TYPO3 administrators and developers alike it lets you manage simple things like development mode toggle, custom and page specific purging right from your TYPO3 Backend.



Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/typo3-cloudflare-extension
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-cloudflare
	- Front End Demo: https://demo.t3planet.com/t3-extensions/cloudflare
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support
