.. include:: ../Includes.txt

.. _installation:

===============================
How to know the Latest Version?
===============================

Whenever Team T3Planet will release new version for paricular for TYPO3 product, You will get an email information about new released version. Here is the sample email.

.. figure:: Images/NewVersionUpdate.jpeg
   :alt: New Version Update Email

Otherwise, You can easily check if new update is available for your purchased TYPO3 product with below steps.

**Step 1.** Go to NITSAN > License Manager

**Step 2.** Check Your Product, Is "Latest Version" Marked? Then, you don't need to do anything.

.. figure:: Images/UptoDateVersion.jpeg
   :alt: Is New Update Availabe?

**Step 3.** Click on "Check For Updates". If new version update found then you "Update to x.x.x" button will enable

.. figure:: Images/GetLatestVersion.jpeg
   :alt: Get New Version

.. attention::
   
   1. Before update your TYPO3 product, Please make sure to get latest version of EXT.ns_license https://extensions.typo3.org/extension/ns_license
   2. We highly recommend to take a backup (code & database) of your whole TYPO3 instance. During the update, If any problem cause then you can roll back your TYPO3 instance. So, please take backup now before update :)


Update for Non-Composer TYPO3 Instance
======================================

Go here https://docs.t3planet.com/en/latest/License/UpdateVersion/Non-Composer/Index.html

Update for Composer-based TYPO3 Instance
======================================

Go here https://docs.t3planet.com/en/latest/License/UpdateVersion/Composer/Index.html