.. include:: ../Includes.txt

=============
Configuration
=============

Please follow below step to configuration,

.. figure:: Images/set_access_token.png
   		:alt: Constant editor


- **Step 1:** Go to Template/TypoScript

- **Step 2:** Select root page

- **Step 3:** Select constant Editor

- **Step 4:** Select > PLUGIN.tx_nshubspot

- **Step 5:** Add hubspot API URL

- **Step 6:** Add hubspot Client API Key

- **Step 7:** Add Client ID


Pleae follow below steps for hubspot configuration.

HubSpot Configuration
=======================

- **Step 1:** Create your new account in HubSpot: https://app.hubspot.com/developer/

- **Step 2:** After Login Open your application module

.. figure:: Images/Form.png
   		:alt: Create Typo3 Form

- **Step 3:** Get your "HubSpot API key" and "Client ID" from the "Get HubSpot API key" 

.. figure:: Images/get_api_key.png
   		:alt: get api

Generate Client API Key and Client ID, here is referance link for step by Step guidance https://legacydocs.hubspot.com/docs/faq/how-do-i-create-an-app-in-hubspot

After API Key and Client ID genaration,configure it in **contants**

Create your HubSpot form
=========================

Create your hubspot form from https://app.hubspot.com/forms/{your_account_id}

.. figure:: Images/create_form.png
   		:alt: hubspot form


Create Typo3 form from Backend and configure with hubspot
=============================================================

- **Step 1:** Enable **"Enable HubSpot"**,Add **HubSpot Portal ID** and **HubSpot Form Id** and also add the **"HubSpot Finisher"** finisher

.. figure:: Images/typo3_form.png
   		:alt: Create Typo3 Form


- **Step 2:** Add Hubspot Value **[add field name same as in HubSpot API]** 

.. figure:: Images/hubspot_value.png
   		:alt: Typo3 Form








