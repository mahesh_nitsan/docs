.. include:: ../Includes.txt

.. _installation:

============
Installation
============

Just install this extension the usual way like any other TYPO3 extension..


For Premium Version - License Activation
========================================

TTo activate the licence and install this premium TYPO3 product, Please refer to this documentation.

**Step 1:** Purchase the Extension from the link, then Login to your TYPO3 Backend and Navigate to the "Extension Manager" module.

**Step 2:** Select “Get the extension” from the dropdown menu.

**Step 3:** Search with Extension Key "ns_license", and install the Licence Manager, now enter the licence key received from your registered Email ID. 

**Step 4:** Now install “ns_t3ai” just like the License Manager. By clicking on the " Retrieve/Update " Button, to Import Extension From repository. 
Refer documentation for the detailed view https://docs.t3planet.com/en/latest/License/Index.html


For Free Version
================

In the TYPO3 Backend, you can simply use the extension manager.

**Step 1:** Navigate to the "Extension Manager" module.

**Step 2:** Select “Get the extension” from the dropdown menu.

**Step 3:** Search with Extension Key "ns_t3ai" 

**Step 4:** Click on the " Retrieve/Update " Button, to Import Extension From repository. 

You can Get the Latest Version from typo3.org : https://extensions.typo3.org/extension/ns_t3ai/ by downloading either the t3x or zip version. Upload the file afterwards in the Extension Manager.

.. figure:: Images/installation_final.png
   :alt: Install Extension


How to Install TYPO3 Extension ns_t3ai
===========================================

**Extension Installation Via without Composer mode**
https://youtu.be/SN5HoFQcDM4?si=n47pC_kwuiRH3Aum

**Extension Via Composer**
https://youtu.be/_7ILu4lwU-k?si=YsL5Lk-sZuhZik5_
