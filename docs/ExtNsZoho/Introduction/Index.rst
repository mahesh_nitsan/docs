	
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_zoho
================

.. figure:: Images/ext-ns_zoho.jpeg
   :alt: Extension ns_zoho 

What does it do?
================

EXT:ns_zoho Integrate TYPO3 forms with Zoho CRM to automatically capture leads from your website.Our integration ensures that lead information collected through TYPO3 forms is instantly and accurately synchronized with your Zoho CRM database.



Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/ns-zoho-typo3-extension-free
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-zoho
	- Front End Demo: https://demo.t3planet.com/t3-extensions/zoho
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support
