.. include:: ../Includes.txt

====================
Form Submission API
====================

Before adding plugins to page configure personio APIs and Scheduler!

Please configure this API for Submitting application form successfully

.. figure:: Images/Submit_API.png
   :alt: detail page

**Step:1** Go to Admin tools>Settings

**Step:2** Click on configure extensions

**Step:3** Select ns_personio

**Step:4** Add Application submit API

**Step:5** Add Documents Submit API and save the configuration

====================
Configure Scheduler
====================

Please configure schedular for fatching Data from personio API

.. figure:: Images/Schedular.png
   :alt: Select extension

**Step:1** Go to System>Scheduler

**Step:2** Select Task

**Step:3** Select Schedulable Command

**Step:4** Add Personio Account API,You can get It from https://support.personio.de/hc/en-us/articles/207576365-Integrate-positions-from-Personio-into-your-company-website-via-XML

**Step:5** Add Language Uid from site management, you can create Multiple scheduler according to diffrent languages!

**Step:6** Page ID where to persist new or updated jobs

**All done now you can configure plugins to your pages!**