	.. include:: ../Includes.txt

=========
Zoom View
=========

Once you setup the Zoom View Gallery module, you can use them in Gallery Plugins to display at your site.

.. figure:: Images/zoom_view_plugin_wizard.jpeg
   :alt: Zoom View Gallery plugins wizard

Once you add the plugin, switch to Plugin tab to configure the Zoom View gallery.

.. figure:: Images/add_zoom_view_gallery_plugin.jpeg
   :alt: Add Zoom View Gallery plugin

Zoom View gallery plugin has 2 variations : Single image view & Image Gallery View.


.. Note:: All the configuration options will be same as Album view gallery & self-explanatory. :)