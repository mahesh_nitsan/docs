.. include:: ../Includes.txt

=============
Configuration
=============

Configure Global settings
=========================

You can set all the global settings of this extension at constants. 

- Step 1: Go to Template.

- Step 2: Select root page.

- Step 3: Select Constant Editor

- Step 4:  Select option Plugin.TX_NSINSTAGRAM. Now, you can configure all the options which you want eg., API Configuration, jQuery, etc.

.. figure:: Images/config_1.jpeg
   :alt: Global Configuration

**API Configuration**

To display Instagram Feed and other details, we need to generate Access Token. There are 2 ways to generate the Access Tokens:

- **1. Instagram V1 API Access Token** - This token can be generated from here: https://quadlayers.com/insta-token/

- **2. Instagram Basic Display API Access Token** - This token can be generated from here: https://developers.facebook.com/docs/instagram-basic-display-api/getting-started

.. figure:: Images/api_config.jpeg
   :alt: API Configuration


Configure Instagram Plugin
==========================

First of all, Add Instagram Plugin from Create new content element > Plugins > Instagram Feed

.. figure:: Images/plugin_config.jpeg
   :alt: Plugin Configuration

**- Select Type:** There are 3 ways to display Instagram feed.

	**- Show Feed using Javascript without API:** This option will display Instagram feed without needing any Access Token. You just need to set username/Hashtag to get the feed.

	**- Show Feed using Instagram V1 API:** Use this option if you have Access Token generated using Instagram V1 API. You can set it at Constants.

	**- Show Feed using Instagram Basic Display API:** Use this option if you have Access Token generated using Instagram Basic Display API. You can set it at Constants.

**- Show Gallery From:** You can decide if you want to display feed using username or hashtag.

**- Username:** Set username whose feed you want to display. If you have selected "Hashtag" in above field then this field will be replaced by Hashtag field. You can set hashtag there.

**- Show Listing of :** Here you can decide if you want to show Photos or IGTV videos in the feed.

**- Display Profile:** Check this to display Profile section above feed. (Note: Profile section will be available for Javascript and V1 API types of feed)

**- Display Biography:** Check this to display Biography of user selected above.

**- Display Follow button:** Check this to display Follow button next to username.

**- Display Website:** Check this to display Website set by user on his/her profile.

**- Items:** Set the number of posts you want to display on Feed.

**- Items per Row:** Set posts to display in a single row in Feed.

**- Margin:** Set margin to display b/w posts in Feed.

**- How to display a post on click?:** You can display post either in Lightbox or open post in Instagram site when user click on post.