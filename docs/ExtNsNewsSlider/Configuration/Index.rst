.. include:: ../Includes.txt

=============
Configuration
=============

.. toctree::

	AddNewsSliderPlugin/Index
	RoyalSlider/Index
	NivoSlider/Index
	OwlcarouselSlider/Index
	SlickSlider/Index
