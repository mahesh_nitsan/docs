.. include:: ../Includes.txt


Slick Slider
============

You can configure Following options in Slick Slider:

- Slider Type

- Display Dots

- Is Infinite

- Adaptive Height

- Display Align

- Transmission Speed

- Number of slides to show
