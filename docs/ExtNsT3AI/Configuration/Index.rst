.. include:: ../Includes.txt


===============================
Global Settings for T3AI API
===============================

First of all, you need to add T3AI API key. You can do it by performing following steps:

.. figure:: Images/openai_1.png
   :alt: openstreet_map_Leaflet_Library

.. figure:: Images/openai_2.png
   :alt: openstreet_map_Leaflet_Library


Before starting you will need to add an T3AI API key. To get the same Login to your OpenAI Account then navigate to, https://platform.openai.com/account/api-keys.
Now click on “Create New Secret Key” to create one. As mentioned in the image below.

Before starting you will need to add an T3AI API key. To get the same please refer to their official documentation : https://platform.openai.com/docs/api-reference/introduction

Once you get your API Key, Login to your backend and follow the below steps;

**Step 1**: Navigate to **Admin Tools > Settings > Extension Configuration > ns_t3ai.** 

**Step 2**: Now, add your API key. You can modify other configurations as per your requirements.

AI Engine Settings
===================

**API Key:** (String) Enter your API key.

**Model:** (Options) Choose the AI model. (e.g., gpt-3.5-turbo)

**Temperature:** (Double: 0.0 - 1.0) Control response randomness. (e.g., 0.7)

**Max Tokens:** (Int+) Set the maximum response length. (e.g., 1300)

**Top P:** (Double: 0.0 - 1.0) Adjust response focus. (e.g., 0.01)

**Best Of:** (Int+) Determine the number of completions selected. (e.g., 1)

**Frequency Penalty:** (Double: 0.0 - 1.0) Avoid repetitive content. (e.g., 0.01)

**Presence Penalty:** (Double: 0.0 - 1.0) Discourage overuse of phrases/entities. (e.g., 0.01)


.. figure:: Images/AI.png
   :alt: openstreet_map_Leaflet_Library

**Content:** Select the “Writing Style” and “Writing Tone” Globally which means these settings will be applied by default, when you select the “By default” option in the page property.

**Basic Request Settings:** Select how to want to share requests to T3AI, In terms of URL or content.

**Page Title:** Select the tone of meta titles and enter the number of results you want to generate.

**Meta Description:** Select the tone of meta description and enter the number of results you want to generate.

**Keywords:** Select the default pattern for Meta Keywords along with the number of results.

**Social Media:** Select Open Graph Title and Description tone and enter the number of results you want to generate.

**Topic:** Select the tone of page titles and enter the number of results you want to generate.

**Localization:** Enable/Disable Language Translation option "Translate with T3AI" for page.

**Step 3:** Click on “Save ns_t3ai configurations” to save the same.

AI-Powered Content via TYPO3 RTE:
===================================

Before Generateion of Content from RTE follow below steps to enable it,

.. figure:: Images/RTE_configuration.png
   :alt: Rich text editor1

**Step 1:** Click on Edit Page property

**Step 2:** Go to tab **"Resources"**

**Step 3:** Include **NS t3ai :: Config RTE Preset (ns_t3ai)** in **Include static Page TSconfig (from extensions)** and Save this.

A powerful feature that Helps You to generate content ideas and rich content using your custom prompts, along with advanced options for creating high-quality content.

Simply write the keyword or question you want answered and click the 'Generate' button.it will Show generated content in text box then click on "OK" It will add content in the RTE! There is another tab, “Advance” which will help you modify content as per your requirements. It comes with options like Writing Style, Writing Tone, Model, Temperature and Amount of results. For more refer to their official documentation from here https://platform.openai.com/docs/introduction 

.. figure:: Images/RTE1.png
   :alt: Rich text editor1

.. figure:: Images/RTE2.png
   :alt: Rich text editor1
