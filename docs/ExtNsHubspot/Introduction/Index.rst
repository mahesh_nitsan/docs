	
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_hubspot
================


What does it do?
================

EXT:ns_hubspot Integrate TYPO3 forms with hubspot CRM to automatically capture leads from your website.Our integration ensures that lead information collected through TYPO3 forms is instantly and accurately synchronized with your hubspot CRM database.


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/ns-hubspot-typo3-extension-free
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-hubspot
	- Front End Demo: https://demo.t3planet.com/t3-extensions/hubspot
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support
