.. include:: ../Includes.txt

.. _installation:

==============
Update Version
==============

.. toctree::
   :glob:

   CheckNewVersion/Index
   NonComposer/Index
   Composer/Index
