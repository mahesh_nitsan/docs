
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_all_chat
===============

.. figure:: Images/ns_all-in-one-chat.jpg
   :alt: TYPO3 ns_all-in-one-chat

.. _What-does-it-do:

What does it do?
================

One of the only TYPO3 extension which provides to use most popular chat tool at your website. This TYPO3 extension provides to configure many live chat tools eg., `zopim.com <https://en.zopim.com/register>`_, `livechatinc.com <https://accounts.livechatinc.com/signup/credentials>`_, `purechat.com <https://www.purechat.com/>`_, `livezilla.net <https://www.livezilla.net/installation/en/>`_, `clickdesk.com <https://www.clickdesk.com>`_ , `tidiochat.com <https://www.tidiochat.com/en/register>`_, `visitlead.com <https://visitlead.com/register>`_, `onwebchat.com <https://www.onwebchat.com/signup.php>`_, `userlike.com <https://www.userlike.com/en/>`_ & more will be available in an upcoming version.

Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/chat-typo3-extension
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-all-chat
	- Front End Demo: https://demo.t3planet.com/t3-extensions/all-chat
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support