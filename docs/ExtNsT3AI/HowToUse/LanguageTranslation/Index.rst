.. include:: ../Includes.txt

====================================
Automated Page Language Translation
====================================

This feature allows you to automatically translate pages seamlessly into your preferred TYPO3 language. It not only creates content elements but also translates content into the selected language.

You need to simply perform following steps to translate your content:

**Step 1:** Select language from the dropdown menu, languages from your TYPO3 site configurations will appear here. Click on **“translate with T3AI”**.

**Step 2:** Click on "Translate with AI".

**Step 3:** Click on the **“Next”** button, check boxes you want to translate and then click on **“Finish”**. It will take a few seconds for the corn to run and It’s all Translated!


.. figure:: Images/translate.png
   :alt: metadata
   
.. figure:: Images/translation1.png
   :alt: metadata

.. figure:: Images/translation2.png
   :alt: metadata

.. figure:: Images/translation3.png
   :alt: metadata

Now you can check your Translated content. **It can also Translate the respective file metadata of the images and EXT:news records also can be translated using our extension**
