.. include:: ../Includes.txt


=============================
ChatGPT Sidebar by T3AI
=============================

Utilize the ChatGPT Sidebar for seamless querying, composing emails, condensing web content, and much more, all directly from your Typo3 backend.


.. figure:: Images/sidebar_button.png
   :alt: AIpage

.. figure:: Images/sidebar.png
   :alt: AIpage


