.. include:: ../Includes.txt


Owlcarousel Slider
==================

.. figure:: Images/owlcarousel_slider.png
   :alt: Owlcarousel Slider 

You can configure Following options in Owlcarousel Slider:

- Auto Play

- Pagination Speed

- GoTo First Speed

- Single Item

- Slide Speed

- ItemsDesktop : value like x,x

- ItemsDesktopSmall : value like x,x

- Stop On Hover

- Navigation

- Auto Height

- Lazy Load

- Transition Style







 