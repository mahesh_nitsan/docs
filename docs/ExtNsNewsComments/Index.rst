﻿.. include:: Includes.txt

====================
EXT:ns_news_comments
====================

.. toctree::
   :glob:

   Introduction/Index
   Installation/Index
   UpdateVersion/Index
   NewsCommentModule/Index
   GlobalConfiguration/Index
   CommentPluginSettings/Index
   CommentModeration/Index
   ForDevelopers/Index
   Support
   BuyNow
   