
.. include:: ../Includes.txt

============
Introduction
============


EXT:ns_news_slick
=================

.. figure:: Images/typo3-ext-news-slick-slider.jpg
   :alt: Extension Banner


What does it do?
================

Add functionality to your TYPO3 website to display slick image slider to your news detail page using our TYPO3 plugin ns_news_slick.
Fully responsive, Swipe enabled, Desktop mouse dragging and Infinite looping.
Fully accessible with Autoplay, dots, arrows and much more.
It facilitates various slider options such as single image, multiple and responsive image view slider.
NS_News_Slick is lightweight, extensive, extendable, easy to use and be predestined to use with TYPO3.

Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/news-slick-slider-typo3-extension
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-news-slick
	- Front End Demo: https://demo.t3planet.com/t3t-extensions/news-slick
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support