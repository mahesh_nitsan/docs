
.. include:: ../Includes.txt

============
Introduction
============


EXT:ns_news_comments
====================

.. figure:: Images/typo3-ext-news-news-comment.jpg
   :alt: Extension news comment


What does it do?
================

It is an extension that facilitates visitor to post comment on a specific news, answer to the comments. Comments are the ideal way to deal and stay in touch with your visitors and adherences. This extension is compatible only with News system Extension (EXT:news).


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/news-comment-typo3-extension
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-news-comments
	- Front End Demo: https://demo.t3planet.com/t3-extensions/seo/news-comments
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support