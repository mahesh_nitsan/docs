.. include:: ../Includes.txt

===========================
Comment Plugin on All Pages
===========================

Display Comment Plugin on all pages of your site
================================================

You can display comment plugin on all pages of your website by adding below fluid object in your template file.

::

    <f:cObject typoscriptObjectPath="lib.tx_nscomments.comments" />


That’s it, Now you can enjoy comments of your website visitors :)