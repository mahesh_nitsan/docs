.. include:: ../Includes.txt

==================================
Configuration For Plugins in page
==================================


1.Personio Job List Plugin
==========================

Follow below steps to configure this exension:

**Step 1.** Click on "Plugin" menu

**Step 2.** Create New Content Element > Insert Plugin > Personio Job List

1.1 General
============

.. figure:: Images/JOB_LIST_1.png
   :alt: Select extension

.. figure:: Images/JOB_LIST_2.png
   :alt: Select extension


**Storage page:** Select Storage page id where your API data is stored

**Details Page:** Configure id of detail page

**Application Page:** Configure id of Application page

1.2 Details
============

.. figure:: Images/JOB_LIST_3.png
   :alt: Select extension


**Add Heading and Description**

1.3 Customization
==================

.. figure:: Images/Customization1.png
   :alt: Select extension

.. figure:: Images/Customization2.png
   :alt: Select extension
   
**Dark mode:** You can enable or disable dark mode for listing page

**Pagination:** You can select Type of Pagination

**Item per Page:** You can set Item per page

**List View:** You can select List view type from dropdown

.. figure:: Images/Customization3.png
   :alt: Select extension

**List View>Customization fields:** You can enable of diable Fields for visibility on FE!

**Filter:** You can enable filtration for Job applications

.. figure:: Images/Customization4.png
   :alt: Select extension

**View type:** YOu can select listing view type from Dropdown to render in Filter

**List View:** By this you can show list in Frontend

**Thumbnail:** By this You can show Applications aa thumbnails

**Isotop:** By this you can Show applications in isotop view

**Masonry:** By this you can show application in masonry view

**Apply now:** By this option you can show apply now button on listing page 


2.Personio Job Detail Plugin
=============================

**Step 1.** Click on "Plugin" menu

**Step 2.** Create New Content Element > Insert Plugin > Personio Job Details


.. figure:: Images/Detail_Page_1.png
   :alt: detail page


**Dark Theme:** You can enable/Disable dark mode

**List  Page:** Configure List page id

**Application page:** Configure application Page Id

.. figure:: Images/Detail_page_2.png
   :alt: detail page

**Show Apply button:** By this option you can enable or disable apply now botton in detail page

**Apply Button Position:** By this option you can set Position on top in detail page by enabling it 

**Common content:** You Can select Content elements/Pages for showing in Detail page

3.Personio Jobs Application Plugin
===================================

**Step 1.** Click on "Plugin" menu

**Step 2.** Create New Content Element > Insert Plugin > Personio Job Details


3.1 General
============

.. figure:: Images/Application_1.png
   :alt: detail page


You can configure Comapny id and Access token from your personio account,learn more from https://developer.personio.de/docs/getting-started-with-the-personio-api

**Company id:** Add Company id

**Access token:** Add access token

**List page:** Configure list page id

.. figure:: Images/Application_2.png
   :alt: detail page


**After Successful Application Redirect to Page:** After Successful submition of Application redirected to this configured page

**After Failed Application Redirect to Page:** If Application submission failed it will redirect to this page

3.2 View
=========

.. figure:: Images/Application_3.png
   :alt: detail page

**Dark theme:** You can enable or disble dark theme for application form page

**Show header:** You can Enable/disable header visibility in Frontend

**Application Message:** You can add message for application page

.. figure:: Images/Application_4.png
   :alt: detail page

**Privacy policy URL:** Add Privacy policy URL for application form

**Layout:** You can select Layout of form from Dropdown

Thats..it you can now check your job listing on Your website!
