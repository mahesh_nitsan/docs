.. include:: ../Includes.txt


=========================
Create AI Page
=========================

To create an AI page automatically, simply navigate to the "Create AI Page" tab. Then select the type of page you want to create from the dropdown menu from articles to landing pages and website contact pages, etc. Now enter the keyword and then click on “Generate”.


.. figure:: Images/AI_page.png
   :alt: AIpage


Choose Suggested Page Titles:
==============================

Now select the Title from the recommendations and click on **“Generate”** to generate content headlines. 

Choose Recommended Outline Content:
===================================

Check the content headers and select Element type from dropdown which you want to create and want to integrate as per your requirements then select **“Create elements in an existing page”** or **“Create a new sub-page with elements”** As per your requirements. 

Within seconds you will find all the content elements created and integrated  as per your preference. 