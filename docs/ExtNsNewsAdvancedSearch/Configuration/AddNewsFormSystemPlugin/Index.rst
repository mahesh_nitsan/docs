.. include:: ../Includes.txt


===============================
1. General News System Settings
===============================

Add News System Plugin from Add New Element popup.

=================
1.1 Add News Form 
=================


.. figure:: Images/News_Form_Setting.jpeg
   :alt: General Settings of Advance News Search Plugin

- **News System** -> Select the news form from news system plugin & save it.  Now You can see the "Advance Search Settings" tab in news system plugin.!

===========================
1.2 Advance Search Settings
===========================

.. figure:: Images/Advance_Search_Setting.jpeg
   :alt: Advance search settings in News System Plugin
   
- **Enable Advance Search** -> Enable the checkbox to render the search form at front end.

- **Category List** -> Either Choose first option to display all categories from all system category folder OR Choose second option to displays all category from only selected folders.

That's it.! Advance search form now displaying on your website.