.. include:: ../Includes.txt

=============
Configuration
=============

.. toctree::

	AddMapAPI/Index
	AddMapLocations/Index
	AddGoogleMapPlugin/Index