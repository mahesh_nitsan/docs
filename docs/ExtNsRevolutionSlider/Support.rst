.. include:: Includes.txt

==============
Help & Support
==============

Submit A Ticket
===============
https://t3planet.com/support

Read Official Docs
==================

- Templates Example https://www.sliderrevolution.com/examples/
- User Manual https://www.sliderrevolution.com/manual/
- FAQ https://www.sliderrevolution.com/faq/
- Video Tutorials https://www.sliderrevolution.com/video-tutorials/
- Definition https://www.sliderrevolution.com/definition/
- Checkout More https://www.sliderrevolution.com/

Have an our awesome extension on your TYPO3 website!
