.. include:: ../Includes.txt


Royal Slider
=============


.. figure:: Images/royal_slider.png
   :alt: Select News Slider 

You can configure Following options in Royal Slider:
 
- Slide Width

- Slide Height

- Deeplinking Enabled

- Deeplinking Change

- Transition Type

- Arrows Nav

- Loop

- Keyboard Nav Enabled

- Controls Inside

- ArrowsNav Auto Hide

- Auto Scale Slider

- Navigate By Click

- Auto Play

- Pause on Hover
 
- Pause Delay

- Global Caption

- Arrows Nav Hide On Touch

- Global Caption Inside

- Auto Scale Slider Width

- Auto Scale Slider Height

- Start Slide Id

- Image Scale Mode

- Control Navigation