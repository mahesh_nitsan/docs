	.. include:: ../Includes.txt

===================
Mosaic View gallery 
===================

Once you setup the mosaic view in tab of Gallery module, you can use them in Gallery Plugins to display at your site.

.. figure:: Images/plugin_wizard_mosaicview.jpeg
   :alt: Gallery plugin_wizard_masonryview
   
Once you add the plugin, switch to Plugin tab to configure the gallery. Here, You can set Gallery specific configuration, Lightbox related configuration and masonry layout to display in this gallery.

.. figure:: Images/ns_gallery_mosiac_view.jpeg
   :alt: Add Masonry Plugin

.. Note:: All the configuration options will be same as Album view gallery & self-explanatory. :)