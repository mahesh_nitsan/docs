.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

After Successful Installation You'll see Backend module "WP Migration"

This Module have two tabs,Let's check it one by one how it works,

Import Manager
***********************

This tab is use to Import Pages/News/Blogs.

Please follow below steps to Import Pages/News/Blogs,


.. figure:: Images/Import_manager.png
                :alt: Import
                

- **Step 1:** -> Select WP Migration module from Backend

- **Step 2:** -> Upload CSV file of Pages/News/Blogs

- **Step 3:** -> Select Post type like which you want to import

- **Step 4:** -> Insert Storage Folder Id in which you want to import your Pages/Blogs/News

- **Step 5:** -> Click on Import

- **Download Sample** -> If the user is unsure about the required file format, they can download a sample file.

After Import user can see logs of imported Data.

Log Manager
***************

User can see the logs with Columns Total Records,Total Inserted,Total Update,Record Storage ID and Import date with Success Message.

        .. figure:: Images/Log_manager.png
                :alt: Log

**After Import You can See your Imported data with Media in Folders which you configured while importing**                

Pages
======

**After Import you will see Pages in Folder With Content**

        .. figure:: Images/Page_1.png
                :alt: Page
                

        .. figure:: Images/Page_2.png
                :alt: Page
        

Blogs
======

**After Import you will see Blogs in Folder With Content and Media**

        .. figure:: Images/Blog_1.png
                :alt: Blogs


        .. figure:: Images/Blog_2.png
                :alt: Blogs


News
=====

**After Import you will see News in Folder With Content and Media**

        .. figure:: Images/News_1.png
                :alt: News_1


        .. figure:: Images/News_2.png
                :alt: News_2


        .. figure:: Images/News_3.png
                :alt: News_3


 
**That's it, Now you can enjoy all the benifits of this extension :)**