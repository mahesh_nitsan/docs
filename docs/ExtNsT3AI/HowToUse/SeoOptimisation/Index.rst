.. include:: ../Includes.txt



==================================================================
Generate SEO-optimized Meta Data, keywords, and Social Media Tags
==================================================================

One click SEO-optimize
===========================

One click SEO-optimize Used to generate Meta title,Meta Description and Meta Keywords All at same time!

.. figure:: Images/Oneclick_SEO.png
   :alt: Oneclick_SEO

**Step 1:** Select "Meta Title" tone from drop-down

**Step 2:** Select "Meta Description" tone from drop-down

**Step 3:** Select "Meta Keywords" from drop-down

**Step 4:** click on "Generate All"

**Step 5:** From generated data,select meta title,description and keyword, It comes with SERP simulator to have an exact view of the same.

Our extension offers the below tones,

**Informative:** Providing straightforward, factual information.

**Persuasive:** Convincing the reader to take action.

**Friendly:** Creating a warm and approachable tone.

**Professional:** Maintaining a formal and business-like tone.

**Casual:** Using a relaxed and conversational style.

**Humorous:** Injecting humour or wit into the content.

**Inspirational:** Motivating and uplifting the reader.

**Urgent:** Creating a sense of immediacy and importance.

**Sensational:** Using attention-grabbing language.

**Authoritative:** Conveying expertise and trustworthiness.

**Promotional:** Encourage the reader to take specific action. 


Meta Title & Meta Description:
================================

Automatically generate  meta titles and descriptions with 10+ prompt variations, To modify the tone of the results perform the below steps;

**Step 1:** Select the “Meta Title” tone from the drop-down as per your preference and click on “generate”.

**Step 2:** Select the “Meta Description” tone from the dropdown menu as per your preference and click on the “generate” button.

.. figure:: Images/metadata.png
   :alt: metadata

Now, click on the “Save Changes” button to automatically integrate the meta data to respective content elements. It comes with SERP simulator to have an exact view of the same.

Meta Keywords
===============

For Meta keywords select the variation from the below as per your choice and then click on generate, to integrate select the best fit and then click on “Save Changes”.
It comes with below variations:

**Generate a List of Keywords:** Select to generate a list of keywords related to your content.

**Generate Long-Tailed Keywords:** Select to generate a list of long-tailed keywords for your content.

**SEO Optimised Recommendations:** Select to generate keywords that are seo fit keywords according to your content

**Provide a List of Top-Performing Keywords:** Select to generate top performing keywords for your content

**Suggest Some High-volume, Low-Difficulty Keywords:** Generate keywords that are highly searched but have low difficulty for your content. 

.. figure:: Images/keywords.png
   :alt: keyword

Social Media Tags
====================

You can generate social media tags by following these steps:

**Step 1:** Select title tone from drop down and click on “generate”.

**Step 2:** Select description tone from dropdown and click on “generate”.

.. figure:: Images/socialmedia.png
   :alt: Socialmedia
   
Now, you can set Social Media Tags or Open Graph Tags for your page by clicking on the “Save Changes” button. Additionally it also has a SERP simulator to have an exact view.

