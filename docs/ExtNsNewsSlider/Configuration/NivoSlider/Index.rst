.. include:: ../Includes.txt


Nivo Slider
===========


.. figure:: Images/nivo_slider.png
   :alt: Nivo Slider 

You can configure Following options in Nivo Slider:

- Theme

- Slider Effect

- Slices (For slice animations)

- Box Columns (For box animations)

- Box Rows (For box animations)

- Animation speed (Slide transition speed)

- Pause Time (How long each slide will show)

- Start Slide (Set starting Slide (0 index))

- Pause On Hover (Stop animation while hovering)

- Direction Navigation (Next and Prev navigation)

- Control Navigation (Pagination navigation)

- Control Navigation Thumbs (Use thumbnails for Control Nav)

- Manual Advance (Force manual transitions)

- Previous Text (Prev direction text)

- Next Text (Next direction text)

- Random Start (Start on a random slide)
